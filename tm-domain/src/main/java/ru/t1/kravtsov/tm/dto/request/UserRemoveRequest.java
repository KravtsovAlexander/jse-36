package ru.t1.kravtsov.tm.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserRemoveRequest(final @Nullable String token) {
        super(token);
    }

}
