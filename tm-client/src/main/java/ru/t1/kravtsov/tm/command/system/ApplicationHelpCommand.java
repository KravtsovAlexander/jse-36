package ru.t1.kravtsov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Display list of terminal commands.";

    @NotNull
    public static final String NAME = "help";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
