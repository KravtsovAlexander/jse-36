package ru.t1.kravtsov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.ISessionRepository;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    protected final String COLUMN_DATE = "date";

    @NotNull
    protected final String COLUMN_ROLE = "role";

    public SessionRepository(final @NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return "tm_session";
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(final @NotNull ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString(COLUMN_ID));
        session.setUserId(row.getString(COLUMN_USER_ID));
        session.setDate(row.getTimestamp(COLUMN_DATE));
        session.setRole(Role.valueOf(row.getString(COLUMN_ROLE)));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(final @NotNull Session model) {
        @NotNull final String sql = "INSERT INTO " + getTableName()
                + " ("
                + COLUMN_ID + ", "
                + COLUMN_USER_ID + ", "
                + COLUMN_DATE + ", "
                + COLUMN_ROLE
                + ") "
                + "VALUES (?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getUserId());
            statement.setTimestamp(3, new Timestamp(model.getDate().getTime()));
            statement.setString(4, String.valueOf(model.getRole()));
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    protected void update(final @NotNull Session model) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET "
                + COLUMN_USER_ID + " = ?, "
                + COLUMN_DATE + " = ?, "
                + COLUMN_ROLE + " = ? "
                + " WHERE " + COLUMN_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getUserId());
            statement.setTimestamp(2, new Timestamp(model.getDate().getTime()));
            statement.setString(3, String.valueOf(model.getRole()));
            statement.setString(4, model.getId());
            statement.executeUpdate();
        }
    }

}
