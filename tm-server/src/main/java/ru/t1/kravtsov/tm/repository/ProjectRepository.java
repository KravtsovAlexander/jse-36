package ru.t1.kravtsov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(final @NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return "tm_project";
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(final @NotNull ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString(COLUMN_ID));
        project.setName(row.getString(COLUMN_NAME));
        project.setCreated(row.getTimestamp(COLUMN_CREATED));
        project.setDescription(row.getString(COLUMN_DESCRIPTION));
        project.setUserId(row.getString(COLUMN_USER_ID));
        project.setStatus(Status.toStatus(row.getString(COLUMN_STATUS)));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(final @NotNull Project model) {
        @NotNull final String sql = "INSERT INTO " + getTableName()
                + " ("
                + COLUMN_ID + ", "
                + COLUMN_NAME + ", "
                + COLUMN_CREATED + ", "
                + COLUMN_DESCRIPTION + ", "
                + COLUMN_USER_ID + ", "
                + COLUMN_STATUS
                + ") "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setTimestamp(3, new Timestamp(model.getCreated().getTime()));
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getUserId());
            statement.setString(6, String.valueOf(model.getStatus()));
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void update(final @NotNull Project model) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET "
                + COLUMN_NAME + " = ?, "
                + COLUMN_CREATED + " = ?, "
                + COLUMN_DESCRIPTION + " = ?, "
                + COLUMN_USER_ID + " = ?, "
                + COLUMN_STATUS + " = ? "
                + " WHERE " + COLUMN_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getDescription());
            statement.setString(4, model.getUserId());
            statement.setString(5, String.valueOf(model.getStatus()));
            statement.setString(6, model.getId());
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final List<Project> removedProjects = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName()
                + " WHERE " + COLUMN_USER_ID + " = ? "
                + "AND " + COLUMN_NAME + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, name);
            @NotNull final ResultSet row = statement.executeQuery();
            while (row.next()) {
                @NotNull final Project project = fetch(row);
                removedProjects.add(remove(project));
            }
        }
        return removedProjects;
    }

}
