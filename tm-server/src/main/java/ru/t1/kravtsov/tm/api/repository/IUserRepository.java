package ru.t1.kravtsov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    Boolean doesLoginExist(@NotNull String login);

    @NotNull
    Boolean doesEmailExist(@NotNull String email);

    void update(@NotNull User user);

}
