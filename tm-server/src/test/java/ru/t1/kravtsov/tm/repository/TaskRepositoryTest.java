package ru.t1.kravtsov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.service.IConnectionService;
import ru.t1.kravtsov.tm.api.service.IPropertyService;
import ru.t1.kravtsov.tm.comparator.NameComparator;
import ru.t1.kravtsov.tm.comparator.StatusComparator;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.marker.UnitCategory;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.service.ConnectionService;
import ru.t1.kravtsov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Ignore
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository repository = new TaskRepository(connection);

    @NotNull
    private final Task alphaTask = new Task("alpha", "testTask");

    @NotNull
    private final Task betaTask = new Task("beta", "testTask");

    @NotNull
    private final Task gammaTask = new Task("gamma", "testTask");

    {
        alphaTask.setStatus(Status.COMPLETED);
        alphaTask.setUserId("user1");
        alphaTask.setId("alpha-task-id");
        alphaTask.setProjectId("project-1-id");
        betaTask.setStatus(Status.IN_PROGRESS);
        betaTask.setUserId("user2");
        betaTask.setId("beta-task-id");
        betaTask.setProjectId("project-1-id");
        gammaTask.setStatus(Status.NOT_STARTED);
        gammaTask.setUserId("user1");
        gammaTask.setId("gamma-task-id");
        gammaTask.setProjectId("project-2-id");
    }

    @Before
    public void before() {
        repository.add(alphaTask);
        repository.add(betaTask);
        repository.add(gammaTask);
    }

    @After
    public void after() {
        repository.clear();
    }

    @Test
    public void add() {
        final int initialSize = repository.getSize();
        Assert.assertNotNull(repository.add(new Task()));
        Assert.assertEquals(initialSize + 1, repository.getSize());
    }

    @Test
    public void addCollection() {
        final int initialSize = repository.getSize();
        @NotNull final List<Task> tasks = Arrays.asList(new Task(), new Task());
        Assert.assertNotNull(repository.add(tasks));
        Assert.assertEquals(initialSize + tasks.size(), repository.getSize());
    }

    @Test
    public void addForUser() {
        final int initialSize = repository.getSize();
        @NotNull final Task task = new Task();
        Assert.assertNotNull(repository.add("user1", task));
        Assert.assertEquals(initialSize + 1, repository.getSize());
        Assert.assertEquals("user1", task.getUserId());

        Assert.assertNull(repository.add(null, new Task()));
        Assert.assertNull(repository.add("user1", null));
    }

    @Test
    public void clear() {
    }

    @Test
    public void clearForUser() {
        repository.clear("user1");
        Assert.assertEquals(1, repository.getSize());
        Assert.assertSame(betaTask, repository.findAll().get(0));

    }

    @Test
    public void deleteAll() {
        repository.deleteAll();
        Assert.assertEquals(0, repository.getSize());

    }

    @Test
    public void deleteAllSpecificTasks() {
        @NotNull final List<Task> tasks = Arrays.asList(alphaTask, betaTask);
        repository.deleteAll(tasks);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertSame(gammaTask, repository.findAll().get(0));
    }

    @Test
    public void deleteAllUserTasks() {
        repository.deleteAll("user1");
        Assert.assertEquals(1, repository.getSize());
        Assert.assertSame(betaTask, repository.findAll().get(0));

        @Nullable final String nullId = null;
        repository.deleteAll(nullId);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void findAll() {
        @NotNull List<Task> tasks = repository.findAll();
        @NotNull List<Task> expected = Arrays.asList(alphaTask, betaTask, gammaTask);
        Assert.assertEquals(expected, tasks);

        repository.clear();
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    public void findAllUserTasks() {
        @NotNull List<Task> tasks = repository.findAll("user1");
        @NotNull List<Task> expected = Arrays.asList(alphaTask, gammaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void findAllWithComparator() {
        repository.clear();
        repository.add(betaTask);
        repository.add(gammaTask);
        repository.add(alphaTask);

        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        @NotNull List<Task> tasks = repository.findAll(comparator);
        @NotNull List<Task> expected = Arrays.asList(alphaTask, betaTask, gammaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void findAllUserTasksWithComparator() {
        @NotNull final Comparator comparator = StatusComparator.INSTANCE;
        @NotNull List<Task> tasks = repository.findAll("user1", comparator);
        @NotNull List<Task> expected = Arrays.asList(gammaTask, alphaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void set() {
        @NotNull final List<Task> tasks = Arrays.asList(new Task(), new Task());
        Assert.assertNotNull(repository.set(tasks));
        Assert.assertEquals(tasks, repository.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsById(alphaTask.getId()));
        Assert.assertFalse(repository.existsById("not-existing-task-id"));
    }

    @Test
    public void existsUserTaskById() {
        Assert.assertTrue(repository.existsById("user1", alphaTask.getId()));
        Assert.assertFalse(repository.existsById("user2", alphaTask.getId()));

        Assert.assertFalse(repository.existsById(null, betaTask.getId()));
        Assert.assertFalse(repository.existsById("user1", null));
    }

    @Test
    public void findOneById() {
        Assert.assertSame(alphaTask, repository.findOneById(alphaTask.getId()));
        Assert.assertNull(repository.findOneById("not-existing-task-id"));
    }

    @Test
    public void findOneUserTaskById() {
        Assert.assertSame(alphaTask, repository.findOneById("user1", alphaTask.getId()));
        Assert.assertNull(repository.findOneById("user2", alphaTask.getId()));

        Assert.assertNull(repository.findOneById(null, alphaTask.getId()));
        Assert.assertNull(repository.findOneById("user1", null));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertSame(betaTask, repository.findOneByIndex(1));
        Assert.assertNull(repository.findOneByIndex(999));
    }

    @Test
    public void findOneUserTaskByIndex() {
        Assert.assertSame(betaTask, repository.findOneByIndex("user2", 0));
        Assert.assertNull(repository.findOneByIndex("user2", 1));

        Assert.assertNull(repository.findOneByIndex(null, 0));
        Assert.assertNull(repository.findOneByIndex("user1", null));
    }

    @Test
    public void getSize() {
        Assert.assertEquals(3, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void getSizeOfUserTasks() {
        Assert.assertEquals(2, repository.getSize("user1"));
        Assert.assertEquals(0, repository.getSize(null));
    }

    @Test
    public void remove() {
        @Nullable final Task removedTask = repository.remove(alphaTask);
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(repository.existsById(alphaTask.getId()));

        Assert.assertNull(repository.remove(null));
    }

    @Test
    public void removeUserTask() {
        @Nullable Task removedTask = repository.remove("user1", alphaTask);
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(repository.existsById(alphaTask.getId()));

        removedTask = repository.remove("user1", betaTask);
        Assert.assertNull(removedTask);
        Assert.assertTrue(repository.existsById(betaTask.getId()));

        Assert.assertNull(repository.remove(null, gammaTask));
        Assert.assertNull(repository.remove("user1", null));
    }

    @Test
    public void removeById() {
        @Nullable final Task removedTask = repository.removeById(alphaTask.getId());
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(repository.existsById(alphaTask.getId()));

        Assert.assertNull(repository.removeById("not-existing-task-id"));
    }

    @Test
    public void removeUserTaskById() {
        @Nullable Task removedTask = repository.removeById("user1", alphaTask.getId());
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(repository.existsById(alphaTask.getId()));

        removedTask = repository.removeById("user1", betaTask.getId());
        Assert.assertNull(removedTask);
        Assert.assertTrue(repository.existsById(betaTask.getId()));

        Assert.assertNull(repository.removeById(null, gammaTask.getId()));
        Assert.assertNull(repository.removeById("user2", null));
    }

    @Test
    public void removeByIndex() {
        @Nullable final Task removedTask = repository.removeByIndex(0);
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(repository.existsById(alphaTask.getId()));

        Assert.assertNull(repository.removeByIndex(999));
    }

    @Test
    public void removeUserTaskByIndex() {
        @Nullable Task removedTask = repository.removeByIndex("user1", 1);
        Assert.assertSame(gammaTask, removedTask);
        Assert.assertFalse(repository.existsById(gammaTask.getId()));

        removedTask = repository.removeByIndex("user2", 1);
        Assert.assertNull(removedTask);

        Assert.assertNull(repository.removeByIndex(null, 0));
        Assert.assertNull(repository.removeByIndex("user2", null));
    }

    @Test
    public void removeByName() {
        @NotNull List<Task> removedTasks = repository.removeByName("user1", alphaTask.getName());
        Assert.assertEquals(1, removedTasks.size());
        Assert.assertFalse(repository.existsById(alphaTask.getId()));

        removedTasks = repository.removeByName("user1", "not-existing-task");
        Assert.assertTrue(removedTasks.isEmpty());
    }

    @Test
    public void findAllByTaskId() {
        @NotNull List<Task> tasks = repository.findAllByProjectId("user1", "project-1-id");
        Assert.assertEquals(1, tasks.size());
        Assert.assertSame(alphaTask, tasks.get(0));

        tasks = repository.findAllByProjectId("user1", "not-existing-project-id");
        Assert.assertTrue(tasks.isEmpty());
    }

}
